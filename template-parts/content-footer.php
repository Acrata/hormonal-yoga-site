<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php
$footer_data = get_post_meta($post->ID, 'hy_footer', true);
  $footer_data = maybe_unserialize($footer_data); // nice WP helper function to unserialize if needed
//  echo '<pre>';
//    print_r($footer_data); // output the results
//  echo '</pre>';
// echo gettype($footer_data) . "ssd";
?>
<section class="hy-footer">
<?php
//    print_r($footer_data['texto_descr']);
if(gettype($footer_data)=="array") {
    // for ($x=0;$x <= count($footer_data);$x++) {
        // echo $x;
    
//     ?>
    <div class="content-footer">
        <div class="img-footer">
            <img src="<?php echo $footer_data['upload_footer'][0];?>" alt="some description" />
        </div>
        <div class="text-footer">
            <?php echo $footer_data['texto_footer'];?>
        </div>
    </div>
 <?php 
    }
?>
</section>