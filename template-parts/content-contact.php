<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php
 $theme_options = get_option('my_theme_settings');

  $iconos = $theme_options['text_fa'];
  $correo = $theme_options['contact_hy_email'];
  $facebook = $theme_options['contact_hy_fb'];
  $phone = $theme_options['contact_hy_phone'];
?>
<section id="contact-section-hy" class="hy-contact">
<h2>Contacto</h2>
<div class="contact-icons">
    <?php
    //foreach($iconos as $icono) {
    ?>
    <div class="contact-card-hy">
    <i class="fa <?php echo $correo['icon_code_email'] ?> fa-5x"></i>
    <p><a href="#">contacto@hormonalyoga.org</a></p>
    </div>
    <div class="contact-card-hy">
    <i class="fa <?php echo $facebook['icon_code_fb'] ?> fa-5x"></i>
    <p><a href="#">Facebook</a></p>
    </div>
    <div class="contact-card-hy">
    <i class="fa <?php echo $phone['icon_code_phone'] ?> fa-5x"></i>
    <p><a href="#">XX-XXX-XX-XX-XX</a></p>
    </div>
    <?php //} ?>
    </div>
</section>
