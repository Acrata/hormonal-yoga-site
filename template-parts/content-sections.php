<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php get_template_part( 'template-parts/content', 'quees' ); ?>
<?php get_template_part( 'template-parts/content', 'aboutme' ); ?>
<?php get_template_part( 'template-parts/content', 'courses' ); ?>
<?php get_template_part( 'template-parts/content', 'contact' ); ?>
<?php //get_template_part( 'template-parts/content', 'gallery' ); ?>
<?php get_template_part( 'template-parts/content', 'footer' ); ?>
