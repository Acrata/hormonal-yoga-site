<?php
/*
Title: Opciones sección Clases
Post Type: page
Template: template-intro
order:2
Tab: Clases
Flow: HY Flow
*/
?>
<h2>Clases </h2>
<?php
piklist('field', array(
  'type' => 'editor',
  'field' => 'courses_descr', // This is the field name of the WordPress default editor
  'label' => 'Post Content',
  'template' => 'field', // Only display the field not the label
  'options' => array( // Pass any option that is accepted by wp_editor()
    'wpautop' => true,
    'media_buttons' => true,
    'shortcode_buttons' => true,
    'teeny' => false,
    'dfw' => false,
    'quicktags' => true,
    'drag_drop_upload' => true,
    'tinymce' => array(
      'resize' => false,
      'wp_autoresize_on' => true
    )
  )
));

piklist('field', array(
    'type' => 'group'
    ,'field' => 'hy_courses'
    ,'add_more' => true
    ,'label' => __('Address (Grouped)', 'piklist-demo')
    ,'list' => true
    ,'template' => 'field'
    ,'description' => __('A grouped field with a key set. Data is not searchable, since it is saved in an array.', 'piklist-demo')
    ,'fields' => array(
        array(
            'type' => 'text'
            ,'field' => 'course_title'
            ,'label' => 'Titulo del curso'
            ,'description' => 'Fieldff Description'
            ,'help' => 'Introduce el titulo de la clase a publicar'
            ,'value' => 'Default text'
            ,'attributes' => array(
            'class' => 'course_title'
            )
           )
      ,array(
        'type' => 'datepicker',
        'field' => 'courses_date',
        'label' => 'Date',
        'value' => date('M d, Y', time() + 604800), // set default value
        'options' => array(
          'dateFormat' => 'M d, yy'
        )
      )

    )
    ,'on_post_status' => array(
      'value' => 'lock'
    )
  ));
  piklist('field', array(
   'type' => 'file'
   ,'field' => 'courses_img'
   ,'label' => 'Add File(s)'
   ,'description' => 'This is the media uploader'
   ,'options' => array(
     'modal_title' => 'Add File(s)'
     ,'button' => 'Add'
   )
 ));

 ?>
