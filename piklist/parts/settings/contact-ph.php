<?php
/*
Title:Phone
Order: 32
Setting: my_theme_settings
Tab: Contactos
Flow: HY Options
*/

  // NOTE: If the post_status of an attachment is anything but inherit or private it will NOT be
  // shown on the Media page in the admin, but it is in the database and can be found using query_posts
  // or get_posts or get_post etc....

?>

<div class="piklist-demo-highlight">
  <?php _e('Lista de iconos de font awesome <a target="_blank" href="http://fontawesome.io/icons/">Iconos</a>', 'piklist-demo');?>
</div>
<div class="fb-hy-contact">
</div>

<?php



   piklist('field', array(
       'type' => 'group'
       ,'field' => 'contact_hy_phone'
       ,'label' => __('Iconos y textos de contactos', 'piklist-demo')
       ,'list' => false
       ,'description' => __('Ingresar iconos y textos para la sección de contactos', 'piklist-demo')
       ,'fields' => array(
         array(
           'type' => 'text'
           ,'field' => 'icon_code_phone'
           ,'label' => __('Codigo del icono', 'piklist-demo')
           ,'required' => true
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'fa-phone'
           )
         )
         ,array(
           'type' => 'text'
           ,'field' => 'contact_url_phone'
           ,'label' => __('Link a RRSS')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail, url a RRSS o Texto'
           )
         )
         ,array(
           'type' => 'textarea'
           ,'field' => 'contact_text_phone'
           ,'label' => __('Texto que acompaña al icono')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail, url a RRSS o Texto'
           )
         )


       )
       ,'on_post_status' => array(
         'value' => 'lock'
       )
     ));
