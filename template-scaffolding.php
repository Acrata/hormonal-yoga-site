<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, hormonal_yoga_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'hormonal_yoga_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
