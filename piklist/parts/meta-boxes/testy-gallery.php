<?php
/*
Title: Opciones sección gallery
Post Type: post
Flow: Add Post
Tab: Step 2dd
*/
?>
<h2>Sobre mi</h2>
<?php
piklist('field', array(
        'type' => 'editor'
        ,'field' => 'texto_gallery'
        ,'label' => __('Que es HY?', 'piklist-demo')
        ,'columns' => 7
        ,'attributes' => array(
          'placeholder' => 'City'
        ),
        'options' => array(
          'media_buttons' => true
          ,'wpautop' => false
        )
      )
    );
 ?>
