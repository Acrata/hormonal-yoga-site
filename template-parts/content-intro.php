<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>
<?php
  $theme_options = get_option('my_theme_settings');
 
  $logo_field = $theme_options['hy_logo'];
 
  
 
?>
    <?php 
        get_template_part( 'template-parts/inline', 'cabecera.svg' );
    ?>
    <div class="title-container-hy">
    <!-- <h1 class="site-title-hy">Hormonal Yoga</h1>
    <span>(Hormone Yoga Therapy<span>&#174;</span>)</span> -->
    </div>
<?php the_custom_logo(); ?>
<!-- <header id=hy-header>
    <div class="background-header">
    </div>
</header> -->
 
<nav class="cd-secondary-nav headroom headroom--pinned" id="hy-nav">
    <div class="logo-nav" style="background:#b6838d url('<?php print_r($logo_field[0]);?>') no-repeat center 30%"> 
        
    </div>
	<?php
    wp_nav_menu( array(
        'theme_location' => 'primary',
        'menu_id'        => 'primary-menu',
        'menu_class'     => 'menu dropdown',
    ) );
?>
</nav> <!-- .cd-secondary-nav -->