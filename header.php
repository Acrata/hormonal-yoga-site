<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Hormonal yoga
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
	<script src="https://npmcdn.com/headroom.js@0.9.4/dist/headroom.min.js"></script>
	<?php wp_head(); ?>

	<script src="https://use.fontawesome.com/015185764a.js"></script>
	
</head>

<body <?php body_class(); ?>>
<div id="pasge" class="siste">
	

	<div id="constent" class="site-csontent">
