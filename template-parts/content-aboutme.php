<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php
$about_data = get_post_meta($post->ID, 'hy_about_me', true);
  $about_data = maybe_unserialize($about_data); // nice WP helper function to unserialize if needed
//  echo '<pre>';
//   print_r($about_data); // output the results
//  echo '</pre>';
// echo gettype($about_data) . "ssd";
?>
<section id="about-me" class="hy-aboutme">
<?php
//    print_r($about_data['texto_descr']);
if(gettype($about_data)=="array") {
    // for ($x=0;$x <= count($about_data);$x++) {
        // echo $x;

//     ?>
    <div class="content-aboutme" data-aos="fade-up">

        <div class="img-quees-border" style="background:url(<?php echo $about_data['upload_about_me'][0];?>)no-repeat">
        </div>
        <div class="text-descr">
            <?php echo $about_data['texto_about_me'];?>
        </div>
    </div>
 <?php
    }
?>
<div class="aboutme-cont">
<?php echo wpautop(get_post_meta($post->ID, 'hy_cont_me', true));?>
</div>
</section>
