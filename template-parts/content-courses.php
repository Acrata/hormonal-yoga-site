<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php
$courses_data = get_post_meta($post->ID, 'hy_courses', true);
  $courses_data = maybe_unserialize($courses_data); // nice WP helper function to unserialize if needed
//  echo '<pre>';
//   print_r($courses_data); // output the results
//  echo '</pre>';
// echo gettype($courses_data) . "ssd";
?>
<section id="courses-section-hy" class="hy-courses">
    <div class="inner-courses" data-aos="fade-up">

        <div class="content-desc" >

            <?php echo wpautop(get_post_meta($post->ID, 'courses_descr', true));?>
        </div>
        <div class="courses-list-container">
            <h2>Proximas clases</h2>
<?php
//    print_r($courses_data['texto_descr']);
if(gettype($courses_data)=="array") {
    // for ($x=0;$x <= count($courses_data);$x++) {
        // echo $x;

//     ?>
<ul>
<?php
            foreach ($courses_data as $item_course) {
             ?>
                <li>
                    <h3><?php echo $item_course['course_title']; ?></h3>
                    <div class="course-date"> <?php echo $item_course['courses_date']; ?> </div>
                </li>
             <?php
            }
?>
</ul>
 <?php
}
?>
        </div>
    </div>
</section>
