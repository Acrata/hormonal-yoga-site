<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>

<?php
$gallery_data = get_post_meta($post->ID, 'hy_gallery', true);
  $gallery_data = maybe_unserialize($gallery_data); // nice WP helper function to unserialize if needed
//  echo '<pre>';
//    print_r($gallery_data); // output the results
//  echo '</pre>';
// echo gettype($gallery_data) . "ssd";
?>
<section class="hy-gallery">
<?php
//    print_r($gallery_data['texto_descr']);
if(gettype($gallery_data)=="array") {
    // for ($x=0;$x <= count($gallery_data);$x++) {
        // echo $x;
    
//     ?>
<?php echo wpautop(get_post_meta($post->ID, 'texto_gallery', true)); ?>
    <div class="content-gallery">
        <div class="img-gallery">
            <img src="<?php echo $gallery_data['upload_gallery'][0];?>" alt="some description" />
        </div>
        <div class="text-gallery">
            <?php echo $gallery_data['texto_gallery'];?>
        </div>
    </div>
 <?php 
    }
?>
</section>