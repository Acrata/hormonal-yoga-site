<?php
/*
Title: Correo
Order: 3
Setting: my_theme_settings
Tab: Contactos
Flow: HY Options
*/

  // NOTE: If the post_status of an attachment is anything but inherit or private it will NOT be
  // shown on the Media page in the admin, but it is in the database and can be found using query_posts
  // or get_posts or get_post etc....

?>

<div class="piklist-demo-highlight">
  <?php _e('Lista de iconos de font awesome <a target="_blank" href="http://fontawesome.io/icons/">Iconos</a>', 'piklist-demo');?>
</div>
<div class="fb-hy-contact">
  <h2>Correo electronico</h2>
</div>

<?php



   piklist('field', array(
       'type' => 'group'
       ,'field' => 'contact_hy_email'
       ,'label' => __('Iconos y textos de contactos', 'piklist-demo')
       ,'list' => false
       ,'description' => __('Ingresar iconos y textos para la sección de contactos', 'piklist-demo')
       ,'fields' => array(
         array(
           'type' => 'text'
           ,'field' => 'icon_code_email'
           ,'label' => __('Codigo del icono', 'piklist-demo')
           ,'required' => true
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'fa-envelope'
           )
         )
         ,array(
           'type' => 'email'
           ,'required' => true
           ,'field' => 'contact_email_url'
           ,'label' => __('Correo electronico')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail@hormonalyoga.org'
           )
         )
         ,array(
           'type' => 'textarea'
           ,'required' => true
           ,'field' => 'contact_text_email'
           ,'label' => __('Texto que acompaña al icono')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail, url a RRSS o Texto'
           )
         )


       )
       ,'on_post_status' => array(
         'value' => 'lock'
       )
     ));
