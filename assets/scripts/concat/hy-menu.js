/**
 * File hero-carousel.js
 *
 * Create a carousel if we have more than one hero slide.
 */
window.scriptTheme = {};
( function( window, $, app ) {

	// Constructor.
	app.init = function() {
        AOS.init();
        console.log("hy-menu");
        var secondaryNav = $('.cd-secondary-nav'),
        secondaryNavTopPosition = secondaryNav.offset().top;
     
    $(window).on('scroll', function(){
        // Create a timeline
        var tl = new TimelineLite();
        var $box = $('.custom-logo')
        // Sequence multiple tweens
        // tl.to($box, 1, { x: 50, y: 10, scale:.5 })
        // .to("#primary-menu",.1,{width:"660px",x:122});
        // tl.from(".logo-nav",.8,{x:"0%"} ,1);
        if($(window).scrollTop() > secondaryNavTopPosition ) {
        //  app.simpleParallax(20.4,$(".text-descr"));
        //  app.simpleParallax(-20.4,$(".img-quees"));
            secondaryNav.addClass('is-fixed');	
            setTimeout(function() {
                // secondaryNav.addClass('animate-children');
            //  tl.play();
                $('.custom-logo').addClass('slide-in');
                $('.cd-btn').addClass('slide-in');
            }, 50);
        } else {
            secondaryNav.removeClass('is-fixed');
            setTimeout(function() {
                //  tl.reverse();
                secondaryNav.removeClass('animate-children');
                $('#cd-logo').removeClass('slide-in');
                $('.cd-btn').removeClass('slide-in');
            }, 50);
        }
    });
    

    $(document).ready(function(){
        $('.hy-quees').viewportChecker();
    });
    };

    app.parallaxHy = function() {
        var logger =(() => {
            console.log();
        })();
        //  logger();
       
    }
    app.simpleParallax = ((intensity, element) => {
        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();
            var imgPos = scrollTop / intensity + 'px';
            element.css('transform', 'translateY(' + imgPos + ')');
        });
    });
	

	// Engage!
	$( app.init );
} ( window, jQuery, window.scriptTheme ) );
