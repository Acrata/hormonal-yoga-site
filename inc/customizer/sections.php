<?php
/**
 * Customizer sections.
 *
 * @package Hormonal yoga
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function hormonal_yoga_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'hormonal_yoga_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'hormonal' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'hormonal_yoga_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'hormonal' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'hormonal' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'hormonal_yoga_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'hormonal' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'hormonal_yoga_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'hormonal' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'hormonal_yoga_customize_sections' );
