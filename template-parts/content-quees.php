<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

?>
<?php
$args = array(
	'post_type' => 'page'
);

// $hy_query = new WP_Query( $args );
// print_r($hy_query);
// while($hy_query->have_posts()) : $hy_query->the_post(); ?>
<?php
$group_data = get_post_meta($post->ID, 'hy_quees', true);
$hy_cont = get_post_meta($post->ID, 'hy_cont', true);
  $group_data = maybe_unserialize($group_data); // nice WP helper function to unserialize if needed
//  echo '<pre>';
//   print_r($group_data); // output the results
//  echo '</pre>';
// echo gettype($group_data) . "ssd";
?>
<?php
 $theme_options = get_option('my_theme_settings');
  $bckg_quees = $theme_options['hy_background_quees'];
  ?>
<section id="about-section-hy" class="hy-quees"style="background:url('<?php print_r($bckg_quees[0]);?>') no-repeat">


<?php
if(gettype($group_data)=="array") {
foreach($group_data as $quees_data) {
     ?>
    <div class="content-quees" data-aos="fade-up">
        <div class="text-descr">
            <?php echo $quees_data['texto_descr'];?>
        </div>
        <div class="img-quees-border" style="background:url(<?php echo esc_url($quees_data['upload_descr'][0]);?>)no-repeat">
        </div>
    </div>
<?php }
    }
    ?>
    <div class="cont-hy">
        <p>
            <?php echo wpautop(get_post_meta($post->ID, 'hy_cont', true));?>
        </p>
    </div>

</section>
<?php //endwhile; ?>
<?php //wp_reset_postdata(); // reset the query ?>
