<?php
/*
Title: Opciones sección Footer
Post Type: page
Template: template-intro
order:6
*/
?>
<h2>Sobre mi</h2>
<?php
piklist('field', array(
    'type' => 'group'
    ,'field' => 'hy_footer'
    ,'label' => __('Address (Grouped)', 'piklist-demo')
    ,'list' => false
    ,'description' => __('A grouped field with a key set. Data is not searchable, since it is saved in an array.', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'text'
        ,'field' => 'texto_footer'
        ,'label' => __('Que es HY?', 'piklist-demo')
        ,'columns' => 7
        ,'attributes' => array(
          'placeholder' => 'City'
        )
      )
      ,array(
        'type' => 'file',
        'field' => 'upload_footer',
        'columns' => 5,
        'label' => 'Add File(s)',
        'description' => 'This is the basic upload field.',
        'options' => array(
          'basic' => false, // set field to basic uploader
          'save' => 'url'
        )
      )

    )
    ,'on_post_status' => array(
      'value' => 'lock'
    )
  ));
 ?>
