<?php
/*
Title: HHH Upload Fields
Order: 30
<<<<<<< HEAD
Flow: Contacts
Tab: Correo
Setting: my_theme_settings
=======
Setting: my_theme_settings
Tab: Opciones
Flow: HY Options
>>>>>>> 822cb4840994ced9736fd58c46675597f71af9a8
*/

  // NOTE: If the post_status of an attachment is anything but inherit or private it will NOT be
  // shown on the Media page in the admin, but it is in the database and can be found using query_posts
  // or get_posts or get_post etc....

?>

<div class="piklist-demo-highlight">
  <?php _e('Piklist comes standard with two upload fields: Basic and Media. The Media field works just like the standard WordPress media field, while the Basic uploader is great for simple forms.', 'piklist-demo');?>
</div>

<?php

piklist('field', array(
    'type' => 'file'
    ,'field' => 'hy_logo'
    ,'scope' => 'post_meta'
    ,'label' => __('Subir logo', 'piklist-demo')
    ,'options' => array(
      'modal_title' => __('Add File(s)', 'piklist-demo')
      ,'button' => __('Add', 'piklist-demo')
      ,'save' => 'url'
    )
  ));

  piklist('field', array(
    'type' => 'file'
    ,'field' => 'hy_background_quees'
    ,'scope' => 'post_meta'
    ,'label' => __('Background seccion que es?', 'piklist-demo')
    ,'options' => array(
      'modal_title' => __('Add File(s)', 'piklist-demo')
      ,'button' => __('Add', 'piklist-demo')
      ,'save' => 'url'
    )
  ));
