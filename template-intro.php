<?php
/**
 * Template Name: Home
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Hormonal yoga
 */

get_header(); ?>
<?php get_template_part( 'template-parts/content', 'intro' ); ?>
<main class="cd-main-content">
<?php get_template_part( 'template-parts/content', 'sections' ); ?>	
</main> <!-- .cd-main-content -->

<?php get_footer(); ?>
