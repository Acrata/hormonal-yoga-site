<?php
/*
Title: Opciones sección About Me
Post Type: page
Template: template-intro
order:2
Tab: About Me
Flow: HY Flow
*/
?>
<h2 class="admin-hy-h2">Sobre mi</h2>
<?php
piklist('field', array(
    'type' => 'group'
    ,'field' => 'hy_about_me'
    ,'label' => __('Address (Grouped)', 'piklist-demo')
    ,'list' => false
    ,'template' => 'field'
    ,'description' => __('A grouped field with a key set. Data is not searchable, since it is saved in an array.', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'editor'
        ,'field' => 'texto_about_me'
        ,'label' => __('Que es HY?', 'piklist-demo')
        ,'columns' => 7
        ,'attributes' => array(
          'placeholder' => 'Sobre mi'
        )
        ,'options' => array (
          'wpautop' => true
          ,'media_buttons' => false
          ,'tabindex' => ''
          ,'editor_css' => ''
          ,'editor_class' => ''
          ,'teeny' => false
          ,'dfw' => false
          ,'tinymce' => true
          ,'quicktags' => true
        )
      )
      ,array(
        'type' => 'file',
        'field' => 'upload_about_me',
        'columns' => 5,
        'label' => 'Add File(s)',
        'description' => 'This is the basic upload field.',
        'options' => array(
          'basic' => false, // set field to basic uploader
          'save' => 'url'
        )
      )

    )
    ,'on_post_status' => array(
      'value' => 'lock'
    )
  ));
  piklist('field', array(
    'type' => 'editor'
    ,'field' => 'hy_cont_me'
    ,'template' => 'field'
    ,'label' => __('Cont...')
    ,'options' => array (
      'wpautop' => true
      ,'media_buttons' => false
      ,'tabindex' => ''
      ,'editor_css' => ''
      ,'editor_class' => ''
      ,'teeny' => false
      ,'dfw' => false
      ,'tinymce' => true
      ,'quicktags' => true
    )
  ));
 ?>
