<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Hormonal yoga
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'hormonal' ); ?></h2>
</section>
