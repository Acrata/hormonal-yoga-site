<?php
/*
Title:  Phone information
Order: 70
Setting: my_theme_settings
*/

  // NOTE: If the post_status of an attachment is anything but inherit or private it will NOT be
  // shown on the Media page in the admin, but it is in the database and can be found using query_posts
  // or get_posts or get_post etc....

?>

<div class="piklist-demo-highlight">
  <?php _e('Lista de iconos de font awesome <a target="_blank" href="http://fontawesome.io/icons/">Iconos</a>', 'piklist-demo');?>
</div>

<?php

piklist('field', array(
    'type' => 'text'
    ,'field' => 'text_fa'
    ,'add_more' => true
    ,'label' => 'Text Box'
    ,'description' => 'Field Description'
    ,'help' => 'This is help text.'
    ,'value' => 'Default text'
    ,'attributes' => array(
    'class' => 'text'
    )
   ));

   piklist('field', array(
       'type' => 'group'
       ,'field' => 'contact_hy'
       ,'add_more' => true
       ,'label' => __('Iconos y textos de contactos', 'piklist-demo')
       ,'list' => false
       ,'description' => __('Ingresar iconos y textos para la sección de contactos', 'piklist-demo')
       ,'fields' => array(
         array(
           'type' => 'text'
           ,'field' => 'icon_code'
           ,'label' => __('Codigo del icono', 'piklist-demo')
           ,'required' => true
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'fa-phone'
           )
         )
         ,array(
           'type' => 'url'
           ,'field' => 'contact_url'
           ,'label' => __('Link a RRSS')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail, url a RRSS o Texto'
           )
         )
         ,array(
           'type' => 'textarea'
           ,'field' => 'contact_text'
           ,'label' => __('Texto que acompaña al icono')
           ,'columns' => 6
           ,'attributes' => array(
             'placeholder' => 'mail, url a RRSS o Texto'
           )
         )
        ,array(
    'type' => 'checkbox'
    ,'field' => 'show_hide_checkbox'
    ,'label' => __('Checkbox: toggle a field', 'piklist-demo')
    ,'choices' => array(
      'show' => 'Show'
    )
  )
  ,array(
    'type' => 'text'
    ,'field' => 'show_hide_field_checkbox'
    ,'label' => __('Show/Hide Field', 'piklist-demo')
    ,'description' => __('This field is toggled by the Checkbox field above', 'piklist-demo')
    ,'conditions' => array(
      array(
        'field' => 'show_hide_checkbox'
        ,'value' => 'show'
      )
    )
  )
       )
       ,'on_post_status' => array(
         'value' => 'lock'
       )
     ));
