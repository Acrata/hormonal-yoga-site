<?php
/*
Title: Opciones sección Aboutaa
Post Type: page
Template: template-intro
order:1
Tab: About HY
Flow: HY Flow
*/
?>
<h2>Hola</h2>
<?php
piklist('field', array(
    'type' => 'group'
    ,'field' => 'hy_quees'
    ,'label' => __('Address (Grouped)', 'piklist-demo')
    ,'list' => false
    ,'add_more' => true
    ,'description' => __('A grouped field with a key set. Data is not searchable, since it is saved in an array.', 'piklist-demo')
    ,'fields' => array(

      array(
        'type' => 'editor'
        ,'field' => 'texto_descr'
        ,'label' => __('Que es HY?', 'piklist-demo')
        ,'columns' => 7
        ,'attributes' => array(
          'placeholder' => 'City'
        )
        ,'options' => array (
          'wpautop' => true
          ,'media_buttons' => false
          ,'tabindex' => ''
          ,'editor_css' => ''
          ,'editor_class' => ''
          ,'teeny' => false
          ,'dfw' => false
          ,'tinymce' => true
          ,'quicktags' => true
        )
      )
      ,array(
        'type' => 'file',
        'field' => 'upload_descr',
        'columns' => 5,
        'label' => 'Add File(s)',
        'description' => 'This is the basic upload field.',
        'options' => array(
          'basic' => false, // set field to basic uploader
          'save' => 'url'
        )
      )

    )
    ,'on_post_status' => array(
      'value' => 'lock'
    )
  ));

  piklist('field', array(
    'type' => 'editor'
    ,'field' => 'hy_cont'
    ,'template' => 'field'
    ,'label' => __('Cont...')
    ,'options' => array (
      'wpautop' => true
      ,'media_buttons' => false
      ,'tabindex' => ''
      ,'editor_css' => ''
      ,'editor_class' => ''
      ,'teeny' => false
      ,'dfw' => false
      ,'tinymce' => true
      ,'quicktags' => true
    )
  ));

 ?>
